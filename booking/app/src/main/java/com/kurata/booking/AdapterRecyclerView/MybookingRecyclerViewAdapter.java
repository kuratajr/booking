package com.kurata.booking.AdapterRecyclerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kurata.booking.data.model.Booking;
import com.kurata.booking.data.model.Room;
import com.kurata.booking.databinding.MybookingBinding;

import java.util.ArrayList;
import java.util.List;

public class MybookingRecyclerViewAdapter extends RecyclerView.Adapter<MybookingRecyclerViewAdapter.MybookingViewHolder>{
    private List<Booking> list;
    private List<Room> room;
    private MybookingListener mybookingListener;


    public MybookingRecyclerViewAdapter (List<Booking> list, List<Room> room ,MybookingListener mybookingListener) {
        this.list = list;
        this.room = room;
        this.mybookingListener = mybookingListener;
    }

    @NonNull
    @Override
    public MybookingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MybookingViewHolder(
                MybookingBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull MybookingViewHolder holder, int position) {
        holder.setData(list.get(position),mybookingListener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public void setFilteredList(ArrayList<Booking> ListToShow){
        this.list= ListToShow;
        notifyDataSetChanged();
    }

    public static class MybookingViewHolder extends RecyclerView.ViewHolder {
        MybookingBinding binding;

        public MybookingViewHolder(MybookingBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }

        void setData(Booking booking, MybookingListener mybookingListener){

            binding.bookingid.setText(booking.getId());
            binding.txttime.setText(booking.getTimebooking().toString());
            binding.status.setText(booking.getStatus());
            //binding.txtPrice.setText(room.getPrice());
            binding.getRoot().setOnClickListener(v -> {
                Booking model = new Booking();
                Room model1 = new Room();
                mybookingListener.onUserClicked(model,model1);
            });
        }

        void setRoom(Room room){
            binding.txtPrice.setText(room.getPrice());
        }

    }

    public interface MybookingListener {
        void onUserClicked(Booking booking, Room room);
    }
}
