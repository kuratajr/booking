package com.kurata.booking.ui.hoteldetails;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.kurata.booking.AdapterRecyclerView.ViewPagerAdapter;
import com.kurata.booking.R;
import com.kurata.booking.data.model.Hotel;
import com.kurata.booking.databinding.ActivityHotelDetailBinding;
import com.kurata.booking.ui.roomlist.Roomlist;
import com.kurata.booking.ui.search.Search;

public class Hotel_detail extends AppCompatActivity {

    private ActivityHotelDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = ActivityHotelDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        Bundle args = getIntent().getBundleExtra("BUNDLE");
        Hotel object = (Hotel) args.getSerializable("model");

        ViewPagerAdapter adapter = new ViewPagerAdapter(this, object.getImage());
        binding.viewPager.setAdapter(adapter);

        binding.back.setOnClickListener(v-> onBackPressed());

        binding.btnChooseRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Hotel model = new Hotel();
                Bundle bundle = new Bundle();
                Intent i = new Intent(getApplicationContext(), Roomlist.class);
                bundle.putSerializable("model", object);
                i.putExtra("checkin", getIntent().getIntExtra("checkin", -1));
                i.putExtra("checkout", getIntent().getLongExtra("checkout", -1));
                i.putExtra("BUNDLE",bundle);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });
    }
}