package com.kurata.booking.ui.home;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kurata.booking.data.model.Hotel;
import com.kurata.booking.data.model.Popular;
import com.kurata.booking.data.model.Room;
import com.kurata.booking.data.repo.HotelRepo;

import java.util.List;

public class HomeViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<List<Hotel>> mHotels;
    private MutableLiveData<List<Popular>> mHotelID;
    private MutableLiveData<List<String>> mhotelID;
    private MutableLiveData<List<Room>> mRooms;
    private HotelRepo mRepository;

    public void init(){
        if (mHotels!=null || mRooms != null || mHotelID != null || mhotelID !=null){
            return;
        }
        mRepository = HotelRepo.getInstance();

    }


    public LiveData<List<Hotel>> getHotelPopular(){

        return mHotels=mRepository.getHotelPopular();
    }

    public LiveData<List<Popular>> getHotelID(){
        return mHotelID=mRepository.getPop();
    }

    public LiveData<List<String>> HotelID(){
        return mhotelID=mRepository.getPopz();
    }

    public LiveData<List<Hotel>> HotelSearch(String search){
        return mHotels=mRepository.getHotelActivate(search);
    }

    public LiveData<List<Room>> getRoom(String hotel_id){
        return mRooms=mRepository.getAllRoom(hotel_id);
    }

}