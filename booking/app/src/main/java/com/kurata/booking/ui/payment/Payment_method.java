package com.kurata.booking.ui.payment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;

import com.kurata.booking.R;
import com.kurata.booking.databinding.ActivityBookingDetailBinding;
import com.kurata.booking.databinding.ActivityPaymentMethodBinding;
import com.kurata.booking.ui.roomlist.Roomlist;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Payment_method extends AppCompatActivity {

    private ActivityPaymentMethodBinding binding;
    private Date checkin;
    private long remainingtime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = ActivityPaymentMethodBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        new CountDownTimer(1000000, 1000) {

            public void onTick(long millisUntilFinished) {
               // binding.timePm.setText("00 :" + millisUntilFinished / 1000);
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                binding.timePm.setText("TIME : " + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
                remainingtime=millisUntilFinished;
            }

            public void onFinish() {


            }
        }.start();

        checkin = new Date();
        checkin.setDate( getIntent().getIntExtra("checkin",-1));
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");


        binding.txtcheckin.setText(format.format(checkin.getTime()));

        binding.back.setOnClickListener(v-> onBackPressed());
        binding.btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Payment.class);
                i.putExtra("checkin", binding.txtcheckin.getText());
                i.putExtra("time", remainingtime);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });
    }
}