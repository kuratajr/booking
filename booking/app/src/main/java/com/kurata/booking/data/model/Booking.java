package com.kurata.booking.data.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.Date;

public class Booking implements Serializable{
        private String id;
        private String room_id;
        private @ServerTimestamp Date check_in;
        private @ServerTimestamp Date check_out;
        private String status;
        private String guestname;
        private String guestemail;
        private @ServerTimestamp Date guestbirth;
        private @ServerTimestamp Date timebooking;

        public Booking() {

        }

        public Booking(String id, String room_id, Date check_in, Date check_out, String status, String guestname, String guestemail, Date guestbirth, Date timebooking) {
            this.id = id;
            this.room_id = room_id;
            this.check_in = check_in;
            this.check_out = check_out;
            this.status = status;
            this.guestname = guestname;
            this.guestemail = guestemail;
            this.guestbirth = guestbirth;
            this.timebooking = timebooking;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public Date getCheck_in() {
            return check_in;
        }

        public void setCheck_in(Date check_in) {
            this.check_in = check_in;
        }

        public Date getCheck_out() {
            return check_out;
        }

        public void setCheck_out(Date check_out) {
            this.check_out = check_out;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getGuestname() {
            return guestname;
        }

        public void setGuestname(String guestname) {
            this.guestname = guestname;
        }

        public String getGuestemail() {
            return guestemail;
        }

        public void setGuestemail(String guestemail) {
            this.guestemail = guestemail;
        }

        public Date getGuestbirth() {
            return guestbirth;
        }

        public void setGuestbirth(Date guestbirth) {
            this.guestbirth = guestbirth;
        }

        public Date getTimebooking() {
            return timebooking;
        }

        public void setTimebooking(Date timebooking) {
            this.timebooking = timebooking;
        }

}
