package com.kurata.booking.data.repo;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kurata.booking.data.model.Booking;
import com.kurata.booking.data.model.Hotel;
import com.kurata.booking.data.model.Popular;
import com.kurata.booking.data.model.Room;
import com.kurata.booking.utils.Preference;

import java.util.ArrayList;
import java.util.List;

public class HotelRepo {
    private static final String TAG = "HotelRepo";
    private Preference preferenceManager;
    private Hotel hotel = new Hotel();
    private Room room = new Room();
    private Popular popular = new Popular();
    private Booking booking = new Booking();
    private static HotelRepo instance;

    public static HotelRepo getInstance(){
        if (instance==null){
            instance= new HotelRepo();
        }
        return instance;
    }

    //get hotel activate
    public MutableLiveData<List<Hotel>> getHotelActivate(String search) {
        MutableLiveData<List<Hotel>> allHotel = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        firestore.collectionGroup("hotels").whereEqualTo("status", "Activate").whereEqualTo("city", search).addSnapshotListener((value, error) -> {
            if (error != null) return;
            if (value != null) {
                List<Hotel> tempList = new ArrayList<>();
                for (DocumentSnapshot document : value.getDocuments()) {
                    Hotel model = document.toObject(Hotel.class);
                    assert model != null;
                    tempList.add(model);
                }
                allHotel.postValue(tempList);
            }
        });
        return allHotel;
    }

    //get all hotel popular
    public MutableLiveData<List<Hotel>> getHotelPopular() {
        MutableLiveData<List<Hotel>> allHotel = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        List<String> test = new ArrayList<>();
        CollectionReference applicationsRef = firestore.collection("popular");
        DocumentReference applicationIdRef = applicationsRef.document("pop");
        applicationIdRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<String> tempList = new ArrayList<String>();
                DocumentSnapshot document = task.getResult();
                tempList = (List<String>) document.get("hotel_id");
                test.addAll(tempList);
                Log.d("Test hotel ID", test.toString());
                firestore.collectionGroup("hotels").addSnapshotListener((value, error) -> {
                    if (error != null) return;
                    if (value != null) {
                        List<Hotel> temp = new ArrayList<>();
                        for (DocumentSnapshot document1 : value.getDocuments()) {
                            if (test.contains(document1.getId())){
                                Hotel model = document1.toObject(Hotel.class);
                                assert model != null;
                                if(model.getStatus().equals("Activate")){
                                    temp.add(model);
                                    Log.d("TEST HOTEL:", model.toString());
                                }
                            }else
                            {
                                Log.d("TEST HOTEL NO:","");
                            }
                        }
                        allHotel.postValue(temp);
                    }
                });
            }

        });

        return allHotel;
    }


    //get hotel ID
    public MutableLiveData<List<String>> getPopz() {
        MutableLiveData<List<String>> allpop= new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        CollectionReference applicationsRef = firestore.collection("popular");
        DocumentReference applicationIdRef = applicationsRef.document("pop");
        applicationIdRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<String> tempList = new ArrayList<String>();
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    tempList = (List<String>) document.get("hotel_id");
                }
                allpop.postValue(tempList);
            }
        });
        return allpop;
    }

    //get all POP
    public MutableLiveData<List<Popular>> getPop() {
        MutableLiveData<List<Popular>> allPop = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        firestore.collection("popular").addSnapshotListener((value, error) -> {
            if (error != null) return;
            if (value != null) {
                List<Popular> tempList = new ArrayList<>();
                for (DocumentSnapshot document : value.getDocuments()) {
                    Popular model = document.toObject(Popular.class);
                    assert model != null;
                    tempList.add(model);
                }
                allPop.postValue(tempList);
                Log.d("POPOP", tempList.toString());
            }
        });
        return allPop;
    }


    //get all hotel popular
    public MutableLiveData<List<Hotel>> getHotelCity() {
        MutableLiveData<List<Hotel>> allHotel = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        List<String> test = new ArrayList<>();
        CollectionReference applicationsRef = firestore.collection("popular");
        DocumentReference applicationIdRef = applicationsRef.document("pop");
        applicationIdRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<String> tempList = new ArrayList<String>();
                DocumentSnapshot document = task.getResult();
                tempList = (List<String>) document.get("hotel_id");
                test.addAll(tempList);
                Log.d("Test hotel ID", test.toString());
                firestore.collectionGroup("hotels").addSnapshotListener((value, error) -> {
                    if (error != null) return;
                    if (value != null) {
                        List<Hotel> temp = new ArrayList<>();
                        for (DocumentSnapshot document1 : value.getDocuments()) {
                            if (test.contains(document1.getId())){
                                Hotel model = document1.toObject(Hotel.class);
                                assert model != null;
                                if(model.getStatus().equals("Activate")){
                                    temp.add(model);
                                    Log.d("TEST HOTEL:", model.toString());
                                }
                            }else
                            {
                                Log.d("TEST HOTEL NO:","");
                            }
                        }
                        allHotel.postValue(temp);
                    }
                });
            }

        });

        return allHotel;
    }


    //get all room
    public MutableLiveData<List<Room>> getAllRoom(String hotel_id) {
        MutableLiveData<List<Room>> allRoom = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        //String hoteltype_id, String hotel_id, String roomtype_id
        //whereEqualTo("hoteltype_id", hoteltype_id).whereEqualTo("hotel_id", hotel_id).whereEqualTo("roomtype_id", roomtype_id)
        firestore.collectionGroup("room").whereEqualTo("status", "Activate").whereEqualTo("hotel_id", hotel_id).addSnapshotListener((value, error) -> {
            if (error != null) return;
            if (value != null) {
                List<Room> tempList = new ArrayList<>();
                for (DocumentSnapshot document : value.getDocuments()) {
                    Room model = document.toObject(Room.class);
                    assert model != null;
                    tempList.add(model);
                }
                allRoom.postValue(tempList);
            }
        });
        return allRoom;
    }


    //todo - get booking

    public MutableLiveData<List<Room>> getroom(String userid) {
        MutableLiveData<List<Room>> allRoom = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        List<String> temp = new ArrayList<>();
        firestore.collection("booking").document(userid).collection("order").addSnapshotListener((value, error) -> {
            if (error != null) return;
            if (value != null) {

                for (DocumentSnapshot document : value.getDocuments()) {
                    Booking model = document.toObject(Booking.class);
                    assert model != null;
                    temp.add(model.getRoom_id());
                    Log.d("xxtttromid_test", temp.toString());
                }
            }
        });

        firestore.collectionGroup("room").addSnapshotListener((value, error) -> {
            if (error != null) return;
            if (value != null) {
                List<Room> tempList = new ArrayList<>();
                for (DocumentSnapshot document : value.getDocuments()) {
                    Room model = document.toObject(Room.class);
                    assert model != null;
                    if(temp.contains(model.getId())){
                        tempList.add(model);
                    }
                }
                allRoom.postValue(tempList);
            }
        });

        return allRoom;
    }

    //todo: get Room with Room_id
    public MutableLiveData<List<Booking>> getBooking(String userid) {
        MutableLiveData<List<Booking>> allBooking = new MutableLiveData<>();
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();

        firestore.collection("booking").document(userid).collection("order").addSnapshotListener((value, error) -> {
            if (error != null) return;
            if (value != null) {
                List<Booking> tempList = new ArrayList<>();
                for (DocumentSnapshot document : value.getDocuments()) {
                    Booking model = document.toObject(Booking.class);
                    assert model != null;
                    tempList.add(model);
                }

                allBooking.postValue(tempList);
            }
        });
        return allBooking;
    }



}
