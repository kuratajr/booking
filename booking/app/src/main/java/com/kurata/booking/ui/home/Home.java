package com.kurata.booking.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.kurata.booking.AdapterRecyclerView.PopularRecyclerViewAdapter;
import com.kurata.booking.data.model.Hotel;
import com.kurata.booking.data.model.Popular;
import com.kurata.booking.databinding.FragmentHomeBinding;
import com.kurata.booking.ui.search.Search;
import com.kurata.booking.utils.Constants;
import com.kurata.booking.utils.Preference;

import java.util.ArrayList;

import javax.inject.Inject;

public class Home extends Fragment implements  PopularRecyclerViewAdapter.PopularListener{

    private static final String TAG = "PopularsFragment_Tag";
    ArrayList<Hotel> list = new ArrayList<Hotel>();
    private HomeViewModel mViewModel;
    private FragmentHomeBinding binding;
    private Preference preference;
    ArrayList<Popular> hotelid = new ArrayList<Popular>();
    //RecyclerAdapter --> Hotel
    @Inject
    PopularRecyclerViewAdapter recyclerAdapter;

    public  Home () {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater,container,false);
        //binding = HomeBinding.inflate(inflater,container,false);
        View view = binding.getRoot();
        preference = new Preference(getContext());
        binding.fullName.setText("Hi, "+ preference.getString(Constants.P_FULL_NAME));

        mViewModel =  new ViewModelProvider(requireActivity()).get(HomeViewModel.class);
        mViewModel.init();

        recyclerAdapter = new PopularRecyclerViewAdapter(list,this);

        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false));
        binding.RecyclerView.setAdapter(recyclerAdapter);

        binding.poads.setHasFixedSize(true);
        binding.poads.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false));
        binding.poads.setAdapter(recyclerAdapter);

        binding.poads1.setHasFixedSize(true);
        binding.poads1.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false));
        binding.poads1.setAdapter(recyclerAdapter);


        binding.poads2.setHasFixedSize(true);
        binding.poads2.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false));
        binding.poads2.setAdapter(recyclerAdapter);

        binding.city.setHasFixedSize(true);
        binding.city.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false));
        binding.city.setAdapter(recyclerAdapter);

        mViewModel.getHotelID().observe(getViewLifecycleOwner(),hotelids -> {
            mViewModel.getHotelPopular().observe(getViewLifecycleOwner(), hotels -> {
                list.clear();
                list.addAll(hotels);
                recyclerAdapter.notifyDataSetChanged();
            });
            recyclerAdapter.notifyDataSetChanged();
        });
        binding.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Search.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel =  new ViewModelProvider(requireActivity()).get(HomeViewModel.class);
    }

    @Override
    public void onUserClicked(Hotel hotel) {
        Toast.makeText(getActivity(), "TEST"+ hotel.getName() + hotel.getLocation() + hotel.getAbout(), Toast.LENGTH_SHORT).show();
    }


}