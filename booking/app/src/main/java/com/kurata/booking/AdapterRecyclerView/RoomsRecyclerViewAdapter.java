package com.kurata.booking.AdapterRecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kurata.booking.R;
import com.kurata.booking.data.model.Hotel;
import com.kurata.booking.data.model.Room;
import com.kurata.booking.databinding.ResultRoomListBinding;
import com.kurata.booking.ui.roomlist.Roomlist;

import java.util.ArrayList;
import java.util.List;

public class RoomsRecyclerViewAdapter extends RecyclerView.Adapter<RoomsRecyclerViewAdapter.RoomViewHolder> {
    private List<Room> list;
    private RoomListener roomListener;


    public RoomsRecyclerViewAdapter (List<Room> list, RoomListener roomListener) {
        this.list = list;
        this.roomListener = roomListener;

    }

    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RoomViewHolder(
                ResultRoomListBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder holder, int position) {
        holder.setData(list.get(position), roomListener);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFilteredList(ArrayList<Room> ListToShow){
        this.list= ListToShow;
        notifyDataSetChanged();
    }

    public static class RoomViewHolder extends RecyclerView.ViewHolder {
        ResultRoomListBinding binding;

        public RoomViewHolder(ResultRoomListBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }

        void setData(Room room, RoomListener roomListener){
            ArrayList<String> arrayList = room.getImage();
            binding.roomname.setText(room.getName());
            //Glide.with(binding.image).load(arrayList.get(0).toString()).into(binding.image);
            ViewPagerAdapter adapter = new ViewPagerAdapter(itemView.getContext(), arrayList);
            binding.image.setAdapter(adapter);

            binding.getRoot().setOnClickListener(v -> {
                Room model = new Room();
                model.setId(room.getId());
                model.setName(room.getName());
                model.setAbout(room.getAbout());
                model.setStatus(room.getStatus());
                model.setHotel_id(room.getHotel_id());
                model.setName(room.getName());
                model.setHoteltype_id(room.getHoteltype_id());
                model.setPrice(room.getPrice());
                model.setImage(room.getImage());
                roomListener.onUserClicked(model);
            });

        }
    }

    public interface RoomListener {
        void onUserClicked(Room room);
    }
}
