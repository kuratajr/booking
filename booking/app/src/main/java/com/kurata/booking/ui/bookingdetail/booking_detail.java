package com.kurata.booking.ui.bookingdetail;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.kurata.booking.R;
import com.kurata.booking.databinding.ActivityBookingDetailBinding;
import com.kurata.booking.ui.payment.Payment_method;

import java.text.SimpleDateFormat;
import java.util.Date;

public class booking_detail extends AppCompatActivity {
    private Date checkin, checkout;
    private ActivityBookingDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = ActivityBookingDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        checkin = new Date();
        checkout = new Date();
        checkin.setDate( getIntent().getIntExtra("checkin", -1));
        checkout.setTime( getIntent().getLongExtra("checkout", -1));
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");


        binding.txtCheckin.setText(format.format(checkin.getTime()));
        binding.txtCheckout.setText(format.format(checkout.getTime()));
        binding.back.setOnClickListener(v-> onBackPressed());
        binding.btnChooseRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Payment_method.class);
                i.putExtra("checkin", getIntent().getIntExtra("checkin", -1));
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });
    }
}