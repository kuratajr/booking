package com.kurata.booking.ui.roomlist;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.kurata.booking.AdapterRecyclerView.RoomsRecyclerViewAdapter;
import com.kurata.booking.R;
import com.kurata.booking.data.model.Hotel;
import com.kurata.booking.data.model.Room;
import com.kurata.booking.databinding.ActivityRoomlistBinding;
import com.kurata.booking.ui.home.HomeViewModel;
import com.kurata.booking.ui.room_detail.Room_detail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

public class Roomlist extends AppCompatActivity implements  RoomsRecyclerViewAdapter.RoomListener{

    private ActivityRoomlistBinding binding;
    private Date checkin, checkout;

    ArrayList<Room> list = new ArrayList<Room>();
    private HomeViewModel mViewModel;
    //RecyclerAdapter --> Room
    @Inject
    RoomsRecyclerViewAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = ActivityRoomlistBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //get hotel_id
        Bundle args = getIntent().getBundleExtra("BUNDLE");
        Hotel object = (Hotel) args.getSerializable("model");

        //get date data
        checkin = new Date();
        checkout = new Date();
        checkin.setDate( getIntent().getIntExtra("checkin", -1));
        checkout.setTime( getIntent().getLongExtra("checkout", -1));
        SimpleDateFormat format = new SimpleDateFormat("dd MMM");
        String ckin = format.format(checkin.getTime());
        String ckout = format.format(checkout.getTime());

        //set date
        binding.date.setText(ckin + " - " + ckout);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        binding.back.setOnClickListener(v->onBackPressed());

        mViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        mViewModel.init();
        recyclerAdapter = new RoomsRecyclerViewAdapter(list,this);

        binding.room.setHasFixedSize(true);
        binding.room.setLayoutManager(new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false));
        binding.room.setAdapter(recyclerAdapter);

        mViewModel.getRoom(object.getId()).observe(this, hotels -> {
            list.clear();
            list.addAll(hotels);
            recyclerAdapter.notifyDataSetChanged();
        });

    }

    @Override
    public void onUserClicked(Room room) {
        Intent i = new Intent(getApplicationContext(), Room_detail.class);
        Room model = new Room();
        Bundle bundle = new Bundle();
        model.setId(room.getId());
        model.setName(room.getName());
        model.setAbout(room.getAbout());
        model.setStatus(room.getStatus());
        model.setHotel_id(room.getHotel_id());
        model.setName(room.getName());
        model.setHoteltype_id(room.getHoteltype_id());
        model.setPrice(room.getPrice());
        model.setImage(room.getImage());

        bundle.putSerializable("model", model);
        i.putExtra("checkin", getIntent().getIntExtra("checkin", -1));
        i.putExtra("checkout", getIntent().getLongExtra("checkout", -1));
        i.putExtra("BUNDLE",bundle);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }
}